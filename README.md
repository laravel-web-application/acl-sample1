<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Access Control List
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/acl-sample1.git`
2. Go inside the folder: `cd acl-sample1`
3. Run `cp .env.example .env` then put your DB credential
4. Run `composer install`
5. Run `php artisan migrate:refresh --seed`
6. Run `php artisan serve`
7. Open your favorite browser: http://localhost:8000/
8. Use the following Admin User: admin@konohagakure.co.jp/123456

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Login Page

![Login Page](img/login.png "Login Page")

Dashboard Page

![Dashboard Page](img/dashboard.png "Dashboard Page")

Create Role

![Create Role](img/create-role.png "Create Role")

List Roles

![List Roles](img/list-roles.png "List Roles")

List Users

![List Users](img/list-users.png "List Users")

Create Product

![Create Product](img/create-product.png "Create Product")

List Products

![List Products](img/list-products.png "List Products")

Show Product

![Show Product](img/show-product.png "Show Product") 
